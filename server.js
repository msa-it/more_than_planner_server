const app = require('./app')
const {sync: dbSync} = require('./db/db')
const {sync: routesSync} = require('./routes/index')
const {sync: cronSync} = require('./tools/cron')
const swaggerSync = require('./swagger/index')
const http = require('http')
const debug = require('debug')('server_template:server')
const {PORT = 3334}= require('./config/config')

/**
 * Create HTTP server.
 */
const server = http.createServer(app)

/**
 * Слушатель события "error"
 */
const onError = (error) => {
	if (!error.code && error.original) error = error.original
	if (!['listen', 'connect'].includes(error.syscall)) {
		throw error
	}

	const bind = typeof PORT === 'string'
		? 'Pipe ' + PORT
		: 'Port ' + PORT

	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges')
			process.exit(1)
			break
		case 'EADDRINUSE':
			console.error(bind + ' is already in use')
			process.exit(1)
			break
		case 'ECONNREFUSED':
			console.error(error.message)
			process.exit(1)
			break
		default:
			console.error(error)
			process.exit(1)
	}
}

/**
 * Слушатель события "listening"
 */
const onListening = () => {
	let addr = server.address()
	let bind = typeof addr === 'string'
		? 'pipe ' + addr
		: 'port ' + addr.port
	debug('Listening on ' + bind)
}

/**
 * Инициализация процессов и запуск сервера
 */
const start = async () => {
	try {
		console.log(`Server mode: ${process.env.NODE_ENV}`)
		await dbSync()
		console.log('Database sync finished')
		await routesSync()
		console.log('Routes sync finished')
		await swaggerSync()
		console.log('Swagger sync finished')
		await cronSync()
		console.log('Cron sync finished')
		server.listen(PORT)
		server.on('error', onError)
		server.on('listening', onListening)
	} catch (e) {
		onError(e)
	}
}

start()
	.then(() => {
		console.log('Текущее время: ', Date())
		console.log(`Server listening on port ${PORT}`)
	})
