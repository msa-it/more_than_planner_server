const {
    Joi,
    JoiObject,
    JoiArray
} = require('../../validation/utils')

const userId = Joi.number().integer().positive()

module.exports = Object.freeze({
    setSettings: JoiObject({
        userId,
        taskTransferToTomorrow: Joi.boolean(),
        halfDayTimeFormat: Joi.boolean(),
        dateFormatMDY: Joi.boolean(),
        prioritySort: Joi.boolean(),
        functions: JoiArray(Joi.number().valid(1, 2, 3, 4))
    }),

    getSettings: JoiObject({
        userId
    }),

    appeal: JoiObject({
        userId,
        email: Joi.string().email(),
        message: Joi.string(),
        title: Joi.string()
    })
})
