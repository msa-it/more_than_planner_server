const {User, Setting} = require('../../db/db')
const {MAIL_USER, MAIL_PASSWORD, MAIL_HOST, MAIL_USER_ADDITIONAL} = require('../../config/config')
const nodeMailer = require('nodemailer')
const Moment = require('moment')

module.exports = {
    async appeal({userId, email, message, title}) {
        const user = User.findByPk(userId)

        const transporter = nodeMailer.createTransport({
            host: MAIL_HOST,
            port: 465,
            secure: true,
            auth: {
                user: MAIL_USER,
                pass: MAIL_PASSWORD
            }
        })
        Moment.locale('ru')
        const mailOptions = {
            to: [MAIL_USER, MAIL_USER_ADDITIONAL],
            subject: title,
            text: `ID пользователя: ${userId}\nEmail для ответа: ${email}\n` +
                `Email регистрации: ${user.email}\nДата регистрации: ${Moment(user.createdAt).format('LLLL')}\nСообщение: ${String(message)}`,
            from: MAIL_USER
        }

        return await new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.error(error)
                    reject({message: 'Письмо не может быть отправлено'})
                } else {
                    console.log(info)
                    resolve({message: 'Письмо отправлено'})
                }
            })
        })
    },
    async setSettings({userId, taskTransferToTomorrow, halfDayTimeFormat, dateFormatMDY, prioritySort, functions}) {
        functions = JSON.stringify(functions)
        const setting = await Setting.findOne({where: {userId}})
        if (setting) {
            return {
                result: await setting.update({
                    taskTransferToTomorrow,
                    halfDayTimeFormat,
                    dateFormatMDY,
                    prioritySort,
                    functions
                }),
                ...await module.exports.setLastTimeStamp({userId})
            }
        } else {
            return {
                result: await Setting.create({
                    userId,
                    taskTransferToTomorrow,
                    halfDayTimeFormat,
                    dateFormatMDY,
                    prioritySort,
                    functions
                }),
                ...await module.exports.setLastTimeStamp({userId})
            }
        }
    },
    async getSettings({userId}) {
        return {
            result: await Setting.findOne({where: {userId}, raw: true})
        }
    },
    async getLastTimestamp({userId}) {
        const {lastTimestamp} = await User.findByPk(userId)
        return {lastTimestamp}
    },
    async setLastTimeStamp({userId}) {
        const lastTimestamp = Moment().toISOString()
        await User.update(
            {
                lastTimestamp
            },
            {
                where: {
                    id: userId
                }
            }
        )
        return module.exports.getLastTimestamp({userId})
    },
    async status() {
        return {result: true}
    }
}
