const {BookReview} = require('../../db/db')

module.exports = {
    async getBookReviews () {
        const result = await BookReview.findAll({raw: true})
        return {
            result: result.map(e => {
                return {
                    ...e,
                    link: `https://mtplanner.store/bookReviews/${e.id}/${e.link}`,
                    image: `https://mtplanner.store/bookReviews/${e.id}/${e.image}`
                }
            })
        }
    }
}
