const {Info} = require('../../db/db')
const Moment = require('moment-timezone')

exports.save = async ({externalId, ...data}) => {
    const currentInfo = await Info.findOne({
        where: {
            externalId
        }
    })
    if (data.timezone) {
        data.timezone = Moment().tz(data.timezone).format('Z')
    }
    if (currentInfo) {
        await currentInfo.update(data)
        return currentInfo.get()
    } else {
        return Info.create({...data, externalId})
    }
}
