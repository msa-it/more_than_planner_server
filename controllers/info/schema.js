const {
    Joi,
    JoiObject
} = require('../../validation/utils')

module.exports = Object.freeze({
    save: JoiObject({
        externalId: Joi.string(),
        language: Joi.string(),
        timezone: Joi.string(),
        installedAt: Joi.date().raw(),
        lastVisitedAt: Joi.date().raw(),
        subscribedAt: Joi.date().raw().allow(null),
        subscriptionExpiresAt: Joi.date().raw().allow(null),
        isTrial: Joi.boolean().allow(null),
        firebaseToken: Joi.string()
    })
})
