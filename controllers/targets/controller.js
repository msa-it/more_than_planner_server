const {Target, Task} = require('../../db/db')
const {evadeUndefined} = require('../../tools/tools')
const {setLastTimeStamp} = require('../user/controller')
const Moment = require('moment')
const Op = require('sequelize').Op
const assert = require('assert')

module.exports = {
    async create({userId, fields}) {
        const {
            title,
            text,
            dateEnd,
            externalId,
            parentExternalId,
            active
        } = fields

        assert(
            !(await Target.findOne({
                where: {
                    userId,
                    externalId
                }
            })),
            `Цель с таким externalId уже существует у данного пользователя`
        )
        if (!!parentExternalId) {
            assert(
                !!(await Target.findOne({
                    where: {
                        userId,
                        externalId: parentExternalId
                    }
                })),
                `Несуществующий parentExternalId`
            )
        }
        return {
            result: await Target.create({
                userId,
                title,
                text,
                dateEnd,
                externalId,
                parentExternalId,
                active
            }),
            ...await setLastTimeStamp({userId})
        }
    },
    async update({userId, externalId, id, fields}) {
        const {
            title,
            text,
            dateEnd,
            parentExternalId,
            active
        } = fields

        const target = await Target.findOne({
            where: {
                ...evadeUndefined({
                    id,
                    externalId
                }),
                userId
            }
        })
        if (!!parentExternalId) {
            assert(
                !!(await Target.findOne({
                    where: {
                        userId,
                        externalId: parentExternalId
                    }
                })),
                `Несуществующий parentExternalId`
            )
        }
        return {
            result: await target.update({
                title,
                text,
                dateEnd,
                parentExternalId,
                active
            }),
            ...await setLastTimeStamp({userId})
        }
    },
    async delete({userId, id, externalId}) {
        const target = await Target.findOne({
            where: {
                ...evadeUndefined({
                    id,
                    externalId
                }),
                userId
            }
        })

        await Task.update(
            {
                targetExternalId: null
            },
            {
                where: {
                    targetExternalId: target.externalId
                }
            }
        )

        return {
            result: !!(await Target.destroy({
                where: {
                    ...evadeUndefined({
                        id,
                        externalId
                    }),
                    userId
                }
            })),
            ...await setLastTimeStamp({userId})
        }
    },
    async get({userId, id, externalId}) {
        const target = evadeUndefined(await Target.findOne({
            where: {
                ...evadeUndefined({
                    id,
                    externalId
                }),
                userId
            }
        }))
        target.tasks = evadeUndefined(await Task.findAll({
            where: {
                targetExternalId: target.externalId
            }
        }))

        return {
            result: target
        }
    },
    async list({userId, filter = {}, order = {field: 'id', by: 'desc'}, pagination = {}}) {
        const obj = {
            where: {userId},
            ...pagination,
            order: [[String(order.field), String(order.by)]]
        }
        for (const key in filter) {
            if (!filter.hasOwnProperty(key)) continue
            if (['active'].includes(key)) {
                obj.where[key] = filter[key]
            } else if (['title', 'text', 'parentExternalId'].includes(key)) {
                obj.where[key] = {[Op.like]: `%${filter[key]}%`}
            } else if (['dateEnd'].includes(key)) {
                let day = Moment(filter[key])
                let stringDay = day.format('YYYY-MM-DD')
                obj.where[key] = {[Op.between]: [`${stringDay} 00:00:00`, `${stringDay} 23:59:59`]}
            }
        }
        const list = evadeUndefined(await Target.findAll(obj))
        if (list.length > 0) {
            const tasks = evadeUndefined(await Task.findAll({where: {targetExternalId: list.map(e => e.externalId)}}))
            for (const el of list) {
                el.tasks = tasks.filter(e => e.targetExternalId === el.externalId)
            }
        }
        return {
            result: list
        }
    },
    async updatePack({userId, targets}) {
        const result = []
        for (const target of targets) {
            try {
                const oldTarget = await Target.findOne({
                    where: {
                        externalId: target.externalId,
                        userId
                    }
                })
                if (oldTarget) {
                    result.push(await oldTarget.update(target.fields))
                } else {

                    assert(
                        !(await Target.findOne({
                            where: {
                                userId,
                                externalId: target.externalId
                            }
                        })),
                        `Цель с таким externalId уже существует у данного пользователя`
                    )

                    result.push(await Target.create({
                        ...target.fields,
                        userId,
                        externalId: target.externalId
                    }))
                }
            } catch (e) {
                console.error(e)
                result.push({
                    success: false,
                    info: {
                        externalId: target.externalId,
                        fields: target.fields || null,
                        errors: e
                    }
                })
            }
        }
        return {
            result,
            ...await setLastTimeStamp({userId})
        }
    },
    async deletePack({userId, externalIds}) {
        for (const externalId of externalIds) {
            await Task.update(
                {
                    targetExternalId: externalId
                }, {
                    where: {
                        targetExternalId: externalId
                    }
                }
            )
        }

        const result = []
        for (const externalId of externalIds) {
            result.push(!!(await Target.destroy({
                where: {
                    userId,
                    externalId
                }
            })))
        }
        return {
            result,
            ...await setLastTimeStamp({userId})
        }
    }
}
