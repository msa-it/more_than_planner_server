const Joi   = require('@hapi/joi').extend(require('@hapi/joi-date'))

const userId = Joi.number().integer().positive(),
    text = Joi.string().allow(''),
    title = Joi.string().allow(''),
    dateEnd = Joi.date(),
    id = Joi.number().integer().positive(),
    parentExternalId = Joi.string().allow(null),
    active = Joi.boolean(),
    externalId = Joi.string()

module.exports = Object.freeze({
    create: Joi.object().keys({
        userId: userId.required(),
        fields: {
            title: title.required(),
            text: text.required(),
            dateEnd: dateEnd.required(),
            externalId: externalId.required(),
            parentExternalId: parentExternalId,
            active: active
        }
    }),

    update: Joi.object().keys({
        fields: {
            title: title,
            text: text,
            dateEnd: dateEnd,
            parentExternalId: parentExternalId,
            active: active,
        },
        id,
        externalId: externalId,
        userId: userId.required()
    }).xor('id', 'externalId'),

    delete: Joi.object().keys({
        userId: userId.required(),
        id,
        externalId: externalId
    }).xor('id', 'externalId'),

    get: Joi.object().keys({
        userId: userId,
        id,
        externalId: externalId
    }).xor('id', 'externalId'),

    list: Joi.object().keys({
        filter: {
            title: title,
            text: text,
            dateEnd: dateEnd,
            parentExternalId: parentExternalId,
            active: active,
        },
        order: Joi.object().keys({
            field: Joi.string().valid('dateEnd', 'text', 'title', 'parentExternalId', 'active', 'id', 'externalId').default('externalId'),
            by: Joi.string().valid('asc', 'ASC', 'desc', 'DESC').default('desc')
        }).default({ field: 'createdAt', by: 'desc' }),
        pagination: {
            limit: Joi.number(),
            offset: Joi.number()
        },
        userId: userId.required()
    }),

    updatePack: Joi.object().keys({
        userId: userId.required(),
        targets: Joi.array().items(Joi.object().keys({
            externalId: externalId.required(),
            fields: {
                title: title,
                text: text,
                dateEnd: dateEnd,
                parentExternalId: parentExternalId,
                active: active
            }
        }))
    }),

    deletePack: Joi.object().keys({
        userId: userId.required(),
        externalIds: Joi.array().items(externalId)
    })
})
