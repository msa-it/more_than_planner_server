const {
    Joi,
    JoiObject,
    JoiArray
} = require('../../validation/utils')

const userId = Joi.number().integer().positive(),
    text = Joi.string().allow(''),
    result = Joi.string().allow(''),
    dateStart = Joi.date(),
    dateStartForList = Joi.date().format("YYYY-MM-DD"),
    dateEnd = Joi.date(),
    dateEndForList = Joi.date().format("YYYY-MM-DD"),
    id = Joi.number().integer().positive(),
    typeId = Joi.number().integer(),
    additionalTypeId = Joi.number().integer().allow(null),
    active = Joi.boolean(),
    priorityId = Joi.number(),
    externalId = Joi.string(),
    isAllDay = Joi.boolean(),
    timeNotify = Joi.date().allow(null)

module.exports = Object.freeze({

    create: JoiObject({
        userId: userId.required(),
        fields: {
            priorityId: priorityId,
            additionalTypeId: additionalTypeId,
            typeId: typeId,
            text: text.required(),
            dateStart: dateStart.required(),
            externalId: externalId.required(),
            dateEnd: dateEnd,
            result: result,
            active: active,
            isAllDay: isAllDay,
            timeNotify: timeNotify,
            targetExternalId: externalId.allow(null)
        }
    }),

    update: JoiObject({
        fields: {
            priorityId: priorityId,
            additionalTypeId: additionalTypeId,
            text: text,
            dateStart: dateStart,
            active: active,
            dateEnd: dateEnd,
            result: result,
            isAllDay: isAllDay,
            typeId: typeId,
            timeNotify: timeNotify,
            targetExternalId: externalId.allow(null)
        },
        id,
        externalId: externalId,
        userId: userId.required()
    }).xor('id', 'externalId'),

    delete: JoiObject({
        userId: userId.required(),
        id,
        externalId: externalId
    }).xor('id', 'externalId'),

    get: JoiObject({
        userId: userId,
        id,
        externalId: externalId
    }).xor('id', 'externalId'),

    list: JoiObject({
        filter: {
            priorityId: priorityId,
            additionalTypeId: additionalTypeId,
            typeId: additionalTypeId,
            text: text,
            dateStart: dateStartForList,
            active: active,
            dateEnd: dateEndForList,
            result: result,
            isAllDay: isAllDay,
            timeNotify: timeNotify
        },
        order: JoiObject({
            field: Joi.string().valid('priorityId', 'additionalTypeId', 'typeId', 'text', 'dateStart', 'active', 'dateEnd', 'result', 'id', 'externalId', 'timeNotify').default('externalId'),
            by: Joi.string().valid('asc', 'ASC', 'desc', 'DESC').default('desc')
        }).default({ field: 'createdAt', by: 'desc' }),
        pagination: {
            limit: Joi.number(),
            offset: Joi.number()
        },
        userId: userId.required()
    }),

    updatePack: JoiObject({
        userId: userId.required(),
        tasks: JoiArray(JoiObject({
            externalId: externalId.required(),
            fields: {
                priorityId: priorityId,
                additionalTypeId: additionalTypeId,
                typeId: typeId,
                text: text,
                dateStart: dateStart,
                active: active,
                dateEnd: dateEnd,
                result: result,
                isAllDay: isAllDay,
                timeNotify: timeNotify,
                targetExternalId: externalId.allow(null)
            }
        }))
    }),

    deletePack: JoiObject({
        userId: userId.required(),
        externalIds: JoiArray(externalId)
    })
})
