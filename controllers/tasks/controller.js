const {Task, Type, Priority, AdditionalType, Target} = require('../../db/db')
const {evadeUndefined} = require('../../tools/tools')
const {setLastTimeStamp} = require('../user/controller')
const Moment = require('moment')
const Op = require('sequelize').Op
const assert = require('assert')

module.exports = {
    async create({userId, fields}) {
        const {
            priorityId,
            additionalTypeId,
            typeId,
            text,
            dateStart,
            externalId,
            dateEnd,
            result,
            active,
            isAllDay,
            timeNotify,
            targetExternalId
        } = fields

        assert(
            !(await Task.findOne({
                where: {
                    userId,
                    externalId
                }
            })),
            `Таск с таким externalId уже существует у данного пользователя`
        )

        return {
            result: await Task.create({
                userId,
                priorityId,
                additionalTypeId,
                typeId,
                text,
                dateStart,
                externalId,
                dateEnd,
                result,
                active,
                isAllDay,
                timeNotify,
                targetExternalId
            }),
            ...await setLastTimeStamp({userId})
        }
    },
    async update({userId, fields, id, externalId}) {
        const {
            priorityId,
            additionalTypeId,
            typeId,
            text,
            dateStart,
            dateEnd,
            result,
            active,
            isAllDay,
            timeNotify,
            targetExternalId
        } = fields

        const task = await Task.findOne({
            where: {
                ...evadeUndefined({
                    id,
                    externalId
                }),
                userId
            }
        })
        return {
            result: await task.update({
                priorityId,
                additionalTypeId,
                typeId,
                text,
                dateStart,
                dateEnd,
                result,
                active,
                isAllDay,
                timeNotify,
                targetExternalId
            }),
            ...await setLastTimeStamp({userId})
        }
    },
    async delete({userId, id, externalId}) {
        return {
            result: !!(await Task.destroy({
                where: {
                    ...evadeUndefined({
                        id,
                        externalId
                    }),
                    userId
                }
            })),
            ...await setLastTimeStamp({userId})
        }
    },
    async get({userId, id, externalId}) {
        const task = evadeUndefined(await Task.findOne({
            where: {
                ...evadeUndefined({
                    id,
                    externalId
                }),
                userId
            }
        }))
        if(task.targetExternalId) {
            task.target = await Target.findOne({
                where: {
                    ...evadeUndefined({
                        externalId: task.targetExternalId
                    }),
                    userId
                }
            })
        }
        return {
            result: task
        }
    },
    async list({userId, filter = {}, order = {field: 'id', by: 'desc'}, pagination = {}}) {
        const obj = {
            where: {userId},
            ...pagination,
            order: [[String(order.field), String(order.by)]]
        }
        for (const key in filter) {
            if (!filter.hasOwnProperty(key)) continue
            if (['priorityId', 'active', 'isAllDay', 'additionalTypeId', 'typeId'].includes(key)) {
                obj.where[key] = filter[key]
            } else if (['text', 'result'].includes(key)) {
                obj.where[key] = {[Op.like]: `%${filter[key]}%`}
            } else if (['dateStart', 'dateEnd', 'timeNotify'].includes(key)) {
                let day = Moment(filter[key])
                let stringDay = day.format('YYYY-MM-DD')
                obj.where[key] = {[Op.between]: [`${stringDay} 00:00:00`, `${stringDay} 23:59:59`]}
            }
        }
        const list = evadeUndefined(await Task.findAll(obj))
        if (list.length > 0) {
            const targetList = evadeUndefined(await Target.findAll({where: {externalId: list.map(e => e.targetExternalId)}}))
            for (const el of list) {
                el.target = targetList.find(e => e.externalId === el.targetExternalId)
            }
        }
        return {
            result: list
        }
    },
    async getTypes() {
        const types = await Type.findAll({raw: true})
        const additionalTypes = await AdditionalType.findAll({raw: true})
        return types.map(e => {
            return {
                result: {
                    ...e,
                    additionalTypes: additionalTypes.filter(el => el.typeId === e.id)
                }
            }
        })
    },
    async getPriorities() {
        return {
            result: await Priority.findAll({raw: true})
        }
    },
    async updatePack({userId, tasks}) {
        const result = []
        for (const task of tasks) {
            try {
                const oldTask = await Task.findOne({
                    where: {
                        externalId: task.externalId,
                        userId
                    }
                })
                if (oldTask) {
                    result.push(await oldTask.update(task.fields))
                } else {

                    assert(
                        !(await Task.findOne({
                            where: {
                                userId,
                                externalId: task.externalId
                            }
                        })),
                        `Таск с таким externalId уже существует у данного пользователя`
                    )

                    result.push(await Task.create(
                        {
                            ...task.fields,
                            userId,
                            externalId: task.externalId
                        }
                    ))
                }
            } catch (e) {
                console.error(e)
                result.push({
                    success: false,
                    info: {
                        externalId: task.externalId,
                        fields: task.fields || null,
                        errors: e
                    }
                })
            }
        }
        return {
            result,
            ...await setLastTimeStamp({userId})
        }
    },
    async deletePack({userId, externalIds}) {
        const result = []
        for (const externalId of externalIds) {
            result.push(!!(await Task.destroy({
                where: {
                    userId,
                    externalId
                }
            })))
        }
        return {
            result,
            ...await setLastTimeStamp({userId})
        }
    }
}
