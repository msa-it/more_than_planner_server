const { User, Setting } = require('../../db/db')
const { DEFAULT_SECRET, ANDROID_AUDIENCE } = require('../../config/config')
const jwt = require('jsonwebtoken')
const { evadeUndefined } = require('../../tools/tools.js')
const { OAuth2Client } = require('google-auth-library')

const client = new OAuth2Client(ANDROID_AUDIENCE)

module.exports = {
	async login({ googleToken = '' }) {
		const [tokenId] = googleToken.match(/[\w._-]+/g) || []
		const { id, googleId, email, message } = await module.exports.auth({ tokenId })

		if (!googleId) { throw message }

		return {
			jwt: generateToken(id),
			user: {
				id,
				email,
				googleId
			}
		}
	},
	async sign({ googleId, email }) {
		let user = await User.findOne({
			where: {
				googleId
			}
		})

		if (user) {
			return user
		}

		await User.create({
			googleId,
			email,
			lastTimestamp: null
		})
		user = await User.findOne({
			where: {
				googleId
			}
		})
		await Setting.create({
			userId: user.id
		})
		return user
	},
	async auth ({ tokenId }) {
		try {
			const ticket = await client.verifyIdToken({
				idToken: tokenId,
				audience: ANDROID_AUDIENCE
			})
			const payload = ticket.getPayload()
			const { sub: googleId, email } = payload
			return await module.exports.sign(evadeUndefined({ googleId, email }))
		} catch (e) {
			if (e.message && e.message.includes('Token used too late')) {
				return { success: false, message: 'Token used too late' }
			} else {
				return { success: false, message: 'Unauthorized' }
			}
		}
	}
}

function generateToken(userId) {
	return jwt.sign({
			userId,
		},
		Buffer.from(DEFAULT_SECRET, 'base64')
	)
}
