const {
    Joi,
    JoiObject
} = require('../../validation/utils')

const userDefaults = JoiObject({
    googleToken: Joi.string()
})

module.exports = Object.freeze({

    login: userDefaults

})
