const { DEFAULT_SECRET, DEFAULT_KEYPASS } = require('../config/config')
const jwt = require('express-jwt')
const { evadeUndefined: copy } = require('../tools/tools')
const { validate, Joi } = require('../validation/utils')
const assert = require('assert')

function getParams ({body, query, params}) {
	return {...copy(body), ...copy(query), ...copy(params)}
}

async function prepare ({req, res, handler, validation, isPublic}) {
	const user = (() => {
		if (isPublic) {
			return {}
		}
		const { user } = req
		return user
	})()

	if (!user) {
		res.status(401)
		res.send({ success: false, message: `Unauthorized` })
		return
	}

	if (req.headers['x-keypass'] !== DEFAULT_KEYPASS) {
		res.status(403)
		res.send({ success: false, message: `Forbidden` })
		return
	}

	let params = {
		...getParams(req),
		...user
	}

	if (validation) {
		if (Joi.isSchema(validation)) {
			validation = { schema: validation, options: {} }
		}
		const {
			schema,
			options = {}
		} = validation

		try {
			params = await validate(
				params,
				schema,
				options
			)
		} catch ({ message }) {
			res.status(400)
			res.send({ success: false, message })
			return
		}
	}

	await defaultHandler({ handler, params, req, res })
}

async function defaultHandler ({handler, params, req, res}) {
	try {
		const result = await handler(params, req, res)
		res.send(result)
	} catch (error) {
		let {
			status = 400,
			message
		} = (
			error ||
			{status: 400, message: `Something went wrong`}
		)

		if (error === String(error)) {
			message = error
		}

		res.status(status)
		res.send({
			success: false,
			message
		})
	}
}

async function assign ({app, path, method, handler, validation, isPublic = false}) {
	if (isPublic) {
		try {
			app[method](path, async (req, res) => {
				await prepare({req, res, handler, validation, isPublic: true})
			})
		} catch (e) {
			console.log(e)
		}
	} else {
		try {
			app[method](path, decryptJwt(), async (req, res) => {
				await prepare({req, res, handler, validation})
			})
		} catch (e) {
			console.log(e)
		}
	}
}

function decryptJwt () {
	return jwt({
		secret: new Buffer.from(DEFAULT_SECRET, 'base64'),
		algorithms: ['HS256'],
		getToken: (req) => {
			let token = req.headers['x-authorization']
			if (token) {
				return token
			}
			return null
		}
	})
}

exports.createRoute = (
	{
		path,
		method = 'post',
		handler = async () => {
			console.log('Nice handler, bro')
			return {message: `Empty handler`}
		},
		validation = null,
		isPublic = false
	}) => {

	assert(path, `There are route with no path`)
	return {
		path,
		method,
		handler,
		validation,
		isPublic
	}
}

exports.build = async ({app, routes}) => {
	for (const {path, method, handler, validation, isPublic} of routes) {
		await assign({
			app,
			path,
			method,
			handler,
			validation,
			isPublic
		})
	}
}
