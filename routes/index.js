const app = require('../app')
const { build } = require('./utils')
const glob = require('glob')

exports.sync = async () => {
	try {
		const init = new Promise((resolve, reject) => {
			let routes = []
			glob("routes/routes/*.js", (err, files) => {
				if (err) reject(err)
				try {
					files = files.map(file => {
						[,,file] = file.split('/')
						return file
					})
					for (let file of files) {
						const currentRoutes = require(`./routes/${file}`)
						if (!Array.isArray(currentRoutes)) continue
						routes = [...routes, ...currentRoutes]
					}
				} catch (e) {
					reject(e)
				}
				build({app, routes})
					.then(() => {
						resolve(`Routes assigned`)
					})
					.catch(e => {
						reject(e)
					})
			})
		})
		console.log(await init)
	} catch (e) {
		throw e
	}
}
