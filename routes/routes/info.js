const {createRoute} = require('../utils')
const Controller = require('../../controllers/info/controller')
const Validation = require('../../controllers/info/schema')

module.exports = [
    createRoute({
        path: `/info/save`,
        method: `post`,
        handler: Controller.save,
        validation: Validation.save,
        isPublic: true
    })
]
