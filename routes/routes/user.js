const {createRoute} = require('../utils')
const UserController = require('../../controllers/user/controller')
const UserSchema = require('../../controllers/user/schema')

module.exports = [
	createRoute({
		path: `/user/appeal`,
		method: `post`,
		handler: UserController.appeal,
		validation: UserSchema.appeal,
		isPublic: true
	}),
	createRoute({
		path: `/user/setSettings`,
		method: `post`,
		handler: UserController.setSettings,
		validation: UserSchema.setSettings
	}),
	createRoute({
		path: `/user/getSettings`,
		method: `get`,
		handler: UserController.getSettings,
		validation: UserSchema.getSettings
	}),
	createRoute({
		path: `/user/getLastTimestamp`,
		method: `post`,
		handler: UserController.getLastTimestamp
	}),
	createRoute({
		path: `/status`,
		method: `get`,
		handler: UserController.status,
		isPublic: true
	})
]
