const {createRoute} = require('../utils')
const AuthController = require('../../controllers/auth/controller')
const AuthSchema = require('../../controllers/auth/schema')

module.exports = [
	createRoute({
		path: `/auth/login`,
		method: `post`,
		handler: AuthController.login,
		validation: AuthSchema.login,
		isPublic: true
	})
]
