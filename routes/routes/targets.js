const {createRoute} = require('../utils')
const  TargetsController = require('../../controllers/targets/controller')
const  TargetSchema = require('../../controllers/targets/schema')
module.exports = [
    createRoute({
        path: `/targets/create`,
        method: `post`,
        handler: TargetsController.create,
        validation: TargetSchema.create,
    }),
    createRoute({
        path: `/targets/get/:id`,
        method: `get`,
        handler: TargetsController.get,
        validation: TargetSchema.get,
    }),
    createRoute({
        path: `/targets/getByExternalId/:externalId`,
        method: `get`,
        handler: TargetsController.get,
        validation: TargetSchema.get,
    }),
    createRoute({
        path: `/targets/update`,
        method: `post`,
        handler: TargetsController.update,
        validation: TargetSchema.update,
    }),
    createRoute({
        path: `/targets/delete`,
        method: `post`,
        handler: TargetsController.delete,
        validation: TargetSchema.delete,
    }),
    createRoute({
        path: `/targets/list`,
        method: `post`,
        handler: TargetsController.list,
        validation: TargetSchema.list,
    }),
    createRoute({
        path: `/targets/updatePack`,
        method: `post`,
        handler: TargetsController.updatePack,
        validation: TargetSchema.updatePack,
    }),
    createRoute({
        path: `/targets/deletePack`,
        method: `post`,
        handler: TargetsController.deletePack,
        validation: TargetSchema.deletePack,
    })
]
