const {createRoute} = require('../utils')
const BookReviewsController = require('../../controllers/bookReviews/controller')

module.exports = [
    createRoute({
        path: `/bookReviews`,
        method: `get`,
        handler: BookReviewsController.getBookReviews,
        isPublic: true
    })
]
