const {createRoute} = require('../utils')
const TasksController = require('../../controllers/tasks/controller')
const TasksSchema = require('../../controllers/tasks/schema')

module.exports = [
    createRoute({
        path: `/tasks/create`,
        method: `post`,
        handler: TasksController.create,
        validation: TasksSchema.create
    }),
    createRoute({
        path: `/tasks/update`,
        method: `post`,
        handler: TasksController.update,
        validation: TasksSchema.update
    }),
    createRoute({
        path: `/tasks/delete`,
        method: `post`,
        handler: TasksController.delete,
        validation: TasksSchema.delete
    }),
    createRoute({
        path: `/tasks/get/:id`,
        method: `get`,
        handler: TasksController.get,
        validation: TasksSchema.get
    }),
    createRoute({
        path: `/tasks/getByExternalId/:externalId`,
        method: `get`,
        handler: TasksController.get,
        validation: TasksSchema.get
    }),
    createRoute({
        path: `/tasks/list`,
        method: `post`,
        handler: TasksController.list,
        validation: TasksSchema.list
    }),
    createRoute({
        path: `/tasks/getTypes/`,
        method: `get`,
        handler: TasksController.getTypes
    }),
    createRoute({
        path: `/tasks/getPriorities/`,
        method: `get`,
        handler: TasksController.getPriorities
    }),
    createRoute({
        path: `/tasks/updatePack`,
        method: `post`,
        handler: TasksController.updatePack,
        validation: TasksSchema.updatePack
    }),
    createRoute({
        path: `/tasks/deletePack`,
        method: `post`,
        handler: TasksController.deletePack,
        validation: TasksSchema.deletePack
    })
]
