const devConfig = require('./dev.config')
const prodConfig = require('./prod.config')

const serverConfig = (() => {
	switch(process.env.NODE_ENV) {
		case 'development':
			return devConfig
		case 'production':
			return prodConfig
		default:
			return prodConfig
	}
})()

module.exports = Object.freeze({
	// server
	PORT: 3334,
	SERVER_MODE_DEV: 'development',
	SERVER_MODE_PROD: 'production',

	// auth
	SALT_ROUNDS: 10,
	DEFAULT_SECRET: 'dsfh4t45hnf92jf-[08a]jq]wfhn23sadfm3[491=02u79(&@#_)($%g4',
	DEFAULT_KEYPASS: 'salfljkl3ofh930f]34[fqwopeifmnq[w',
	ANDROID_AUDIENCE: `1084562229706-gobrgfr76139tao0d3c99tbl073fdfrh.apps.googleusercontent.com`,

	MAIL_USER: 'cheebostech@ya.ru',
	MAIL_USER_ADDITIONAL: 'msait.evtushenko@gmail.com',
	MAIL_PASSWORD: 'formorethanplannertechmail',
	MAIL_HOST: 'smtp.yandex.ru',

	// validation
	VALIDATION_SETTINGS: {
		stripUnknown: true,
		errors: {
			escapeHtml: true
		}
	},

	FIREBASE_CREDENTIALS_PATH: '/config/elon-task-firebase-adminsdk-oxemz-a219effb2f.json',

	serverConfig
})
