module.exports = Object.freeze({
	dbConfig: {
		database: 'MoreThanPlanner',
		user: 'root',
		password: 'cbcntvf',
		host: '127.0.0.1',
		port: '3306',
		dialect: 'mysql',
		define: {
			underscored: false,
			dialectOptions: {
				// useUTC: false, // for reading from database
				dateStrings: true,
				typeCast: true
			}
		},
		// timezone: '+03:00', //for writing to database
		timezone: '+00:00', //for writing to database
		logging: (message) => {
			console.log(message)
		}
	}
})
