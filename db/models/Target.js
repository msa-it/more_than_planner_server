const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'target'
const attributes = {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	externalId: {
		type: Sequelize.STRING(256),
		allowNull: false
	},
	active: {
		type: Sequelize.BOOLEAN,
		defaultValue: true
	},
	title: {
		type: Sequelize.STRING(128),
		allowNull: true
	},
	text: {
		type: Sequelize.TEXT
	},
	dateEnd: {
		type: Sequelize.DATE
	},
	parentExternalId: {
		type: Sequelize.STRING(256),
		allowNull: true
	}
}

module.exports = db.define(modelName, attributes)
