const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'setting'
const attributes = {
	taskTransferToTomorrow: {
		type: Sequelize.BOOLEAN,
		defaultValue: false
	},
	halfDayTimeFormat: {
		type: Sequelize.BOOLEAN,
		defaultValue: false
	},
	dateFormatMDY: {
		type: Sequelize.BOOLEAN,
		defaultValue: false
	},
	prioritySort: {
		type: Sequelize.BOOLEAN,
		defaultValue: true
	},
	functions: {
		type: Sequelize.STRING,
		defaultValue: '[1,2,3,4]'
	}
}

module.exports = db.define(modelName, attributes)
