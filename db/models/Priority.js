const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'priority'
const attributes = {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	name: {
		type: Sequelize.STRING(128),
		allowNull: false
	},
	sort: {
		type: Sequelize.INTEGER
	}
}

module.exports = db.define(modelName, attributes, {
	timestamps: false
})
