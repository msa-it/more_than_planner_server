const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'task'
const attributes = {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	externalId: {
		type: Sequelize.STRING(256),
		allowNull: false
	},
	text: {
		type: Sequelize.TEXT,
		allowNull: false
	},
	active: {
		type: Sequelize.BOOLEAN,
		defaultValue: true
	},
	isAllDay: {
		type: Sequelize.BOOLEAN,
		defaultValue: true
	},
	dateStart: {
		type: Sequelize.DATE,
		allowNull: false
	},
	dateEnd: {
		type: Sequelize.DATE
	},
	result: {
		type: Sequelize.TEXT
	},
	timeNotify: {
		type: Sequelize.DATE,
		defaultValue: null
	},
	targetExternalId: {
		allowNull: true,
		type: Sequelize.STRING(256)
	}
}

module.exports = db.define(modelName, attributes)
