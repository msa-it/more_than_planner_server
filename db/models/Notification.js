const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'notification'
const attributes = {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING(255),
    }
}

module.exports = db.define(modelName, attributes)
