const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'info'
const attributes = {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    externalId: {
        type: Sequelize.STRING(511),
        allowNull: false,
        unique: true
    },
    language: {
        type: Sequelize.STRING(15),
        allowNull: true
    },
    timezone: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
    installedAt: {
        type: Sequelize.DATE,
        defaultValue: null,
        allowNull: true
    },
    lastVisitedAt: {
        type: Sequelize.DATE,
        defaultValue: null,
        allowNull: true
    },
    subscribedAt: {
        type: Sequelize.DATE,
        defaultValue: null,
        allowNull: true
    },
    subscriptionExpiresAt: {
        type: Sequelize.DATE,
        defaultValue: null,
        allowNull: true
    },
    isTrial: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: true
    },
    firebaseToken: {
        type: Sequelize.STRING(255),
        allowNull: true
    }
}

module.exports = db.define(modelName, attributes)
