const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'user'
const attributes = {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	googleId: {
		type: Sequelize.STRING(256),
		allowNull: false
	},
	email: {
		type: Sequelize.STRING(128),
		unique: true,
		allowNull: false
	},
	trialDate: {
		type: Sequelize.DATE,
		defaultValue: null
	},
	lastTimestamp: {
		type: Sequelize.DATE,
		defaultValue: null
	}
}

module.exports = db.define(modelName, attributes)
