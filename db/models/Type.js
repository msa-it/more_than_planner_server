const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'type'
const attributes = {
	id: {
		type: Sequelize.INTEGER,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false
	},
	name: {
		type: Sequelize.STRING(128),
		allowNull: false
	}
}

module.exports = db.define(modelName, attributes, {
	timestamps: false
})
