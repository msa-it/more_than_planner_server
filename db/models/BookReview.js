const Sequelize = require('sequelize')
const db = require('../index')

const modelName = 'bookReview'
const attributes = {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING(128),
        allowNull: false
    },
    author: {
        type: Sequelize.STRING(128),
        allowNull: false
    },
    readingLength: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false
    },
    limited: {
        type: Sequelize.INTEGER,
        defaultValue: 0
    },
    link: {
        type: Sequelize.STRING(512)
    },
    image: {
        type: Sequelize.STRING(512)
    },
    storyBackgroundColor: {
        type: Sequelize.STRING(15)
    }
}

module.exports = db.define(modelName, attributes)
