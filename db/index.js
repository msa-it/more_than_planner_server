const Sequelize = require('sequelize')

const {
	database,
	user,
	password,
	host,
	port,
	dialect,
	define,
	timezone,
	logging
} = require('../config/config').serverConfig.dbConfig

const db = new Sequelize(database, user, password, {
	dialect,
	host,
	port,
	define,
	timezone,
	logging
})

module.exports = db
