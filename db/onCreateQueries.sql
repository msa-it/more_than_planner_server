INSERT INTO `types` (`id`, `name`) VALUES
	(1, 'Системные автозадачи');
INSERT INTO `types` (`id`, `name`) VALUES
	(2, 'Пользовательские задачи');

INSERT INTO `additionalTypes` (`id`, `name`, `typeId`) VALUES
	(1, 'Благодарность дня', 1);
INSERT INTO `additionalTypes` (`id`, `name`, `typeId`) VALUES
	(2, 'Цели прочитаны', 1);
INSERT INTO `additionalTypes` (`id`, `name`, `typeId`) VALUES
	(3, 'Что можно было не делать', 1);
INSERT INTO `additionalTypes` (`id`, `name`, `typeId`) VALUES
	(4, 'Отчет за день', 1);

INSERT INTO `priorities` (`id`, `name`, `sort`) VALUES
	(1, 'Главное дело дня', 1);
INSERT INTO `priorities` (`id`, `name`, `sort`) VALUES
	(2, 'Приоритет A', 2);
INSERT INTO `priorities` (`id`, `name`, `sort`) VALUES
	(3, 'Приоритет B', 3);
INSERT INTO `priorities` (`id`, `name`, `sort`) VALUES
	(4, 'Приоритет C', 4);
INSERT INTO `priorities` (`id`, `name`, `sort`) VALUES
	(5, 'Приоритет D', 5);
INSERT INTO `priorities` (`id`, `name`, `sort`) VALUES
	(6, 'Не задан', 6);