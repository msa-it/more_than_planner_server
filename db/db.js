const AdditionalType = require('./models/AdditionalType')
const Priority = require('./models/Priority')
const Setting = require('./models/Setting')
const Target = require('./models/Target')
const Task = require('./models/Task')
const Type = require('./models/Type')
const User = require('./models/User')
const BookReview = require('./models/BookReview')
const Info = require('./models/Info')
const Notification = require('./models/Notification')

const db = require('./index')
const fs = require('fs')

// relations
{
	AdditionalType.hasMany(Task, {as: 'tasks', foreignKey: 'additionalTypeId'})
	Task.belongsTo(AdditionalType, {as: 'additionalType', foreignKey: 'additionalTypeId'})
	Type.hasMany(Task, {as: 'tasks', foreignKey: 'typeId'})
	Task.belongsTo(Type, {as: 'type', foreignKey: 'typeId'})
	Priority.hasMany(Task, {as: 'tasks', foreignKey: 'priorityId'})
	Task.belongsTo(Priority, {as: 'priority', foreignKey: 'priorityId'})
	User.hasMany(Task, {as: 'tasks', foreignKey: 'userId'})
	Task.belongsTo(User, {as: 'user', foreignKey: 'userId'})
	Type.hasMany(AdditionalType, {as: 'additionalTypes', foreignKey: 'typeId'})
	AdditionalType.belongsTo(Type, {as: 'type', foreignKey: 'typeId'})
	User.hasMany(Target, {as: 'targets', foreignKey: 'userId'})
	Target.belongsTo(User, {as: 'user', foreignKey: 'userId'})
	User.hasOne(Setting, {as: 'setting', foreignKey: 'userId'})
	Setting.belongsTo(User, {as: 'user', foreignKey: 'userId'})
	Info.hasMany(Notification, {as: 'notifications', foreignKey: 'infoId'})
    Notification.belongsTo(Info, {as: 'info', foreignKey: 'infoId'})
}

// models
module.exports = {
	AdditionalType,
	Priority,
	Setting,
	Task,
	Target,
	Type,
	User,
	BookReview,
	Notification,
	Info,
	/**
	 * Синхронизация с бд
	 * @param {number} tries
	 * @returns {Promise<void>}
	 */
	async sync (tries = 5) {
		try {
			console.log(await sync())
		} catch (e) {
			try {
				console.log(await createDb())
				console.log(await sync())
				console.log(await setDefaultContent())
			} catch (e) {
				if (tries < 5) {
					await (() => {
						return new Promise((resolve) => {
							setTimeout(() => {
								resolve()
							}, 10000)
						})
					})()
				}
				tries--
				console.error(e)
				if (tries >= 0) {
					console.error(`${tries} more tries before shut down.`)
					await module.exports.sync(tries)
				} else {
					throw e
				}
			}
		}
	}
}


async function sync() {
	try {
		await db.sync()
		return `Database relations prepared`
	} catch (e) {
		throw `Database relations not prepared`
	}
}

async function createDb () {
	try {
		const mysql = require('mysql2/promise')
		const {host, user, password, port, database} = require('../config/config').serverConfig.dbConfig

		const connection = await mysql.createConnection({
			host,
			port,
			user,
			password
		})
		const result = await connection.query(`CREATE DATABASE IF NOT EXISTS \`${database}\` CHARACTER SET utf8 COLLATE utf8_general_ci;`)
		if (!result) {
			return `Database wasn't created`
		}
		return `Database created`
	} catch (e) {
		console.error(e)
		throw `Database wasn't created`
	}
}

async function setDefaultContent () {
	try {
		const mysql = require('mysql2/promise')
		const {host, user, password, port, database} = require('../config/config').serverConfig.dbConfig

		const connection = await mysql.createConnection({
			host,
			port,
			user,
			password,
			database
		})
		const queries = (fs.readFileSync('db/onCreateQueries.sql', 'utf8'))
			.replace(/\n/g, '')
			.replace(/\r/g, '')
			.replace(/\t/g, '')
			.split(';')
			.filter(e => e.length > 0)
		const result = []
		for (const query of queries) {
			try {
				result.push(await connection.query(query))
			} catch (e) {
				console.error(`Query ${query} HAS ERROR:\n${e}`)
			}
		}
		if (result.length) {
			return `Database content added`
		}
		return `Database content wasn't added`
	} catch (e) {
		console.error(e)
		throw `Database content wasn't added`
	}
}
