FROM node:16

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3334

RUN ls

CMD ["npm", "run", "start"]
