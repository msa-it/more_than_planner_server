const cron = require('node-cron')
const {
    Info,
    Notification
} = require('../db/db')
const {Sequelize, Op} = require('sequelize');
const Firebase = require('./firebase')

const jobs = [
    {
        name: 'trialVersionNotification',
        cron: ['0 * * * *'],
        method: async () => {
            try {
                const infos = await Info.findAll({
                    where: {
                        isTrial: true,
                        [Op.and]: [
                            Sequelize.literal(`EXTRACT(HOUR FROM CONVERT_TZ(NOW(), '+00:00', \`info\`.\`timezone\`)) = 19`),
                            Sequelize.literal(`DATEDIFF(NOW(), \`info\`.\`subscribedAt\`) >= 7`),
                            Sequelize.literal(`NOT EXISTS(SELECT * FROM \`notifications\` WHERE \`notifications\`.\`infoId\` = \`info\`.\`id\` AND \`notifications\`.\`name\` = "trialVersionNotification")`)
                        ]
                    },
                    include: [{
                        model: Notification,
                        as: 'notifications',
                        required: false,
                        where: {
                            [Op.not]: {
                                name: 'trialVersionNotification'
                            }
                        }
                    }]
                })
                console.log(infos)
                for (const info of infos) {
                    try {
                        console.log(info)
                        await Firebase.send(
                            info.firebaseToken,
                            {
                                title: 'Напоминание о подписке',
                                body: 'Напоминаем, что у вас оформлена пробная версия приложения Elon Task'
                            }
                        )
                        await Notification.create({
                            infoId: info.id,
                            name: 'trialVersionNotification'
                        })
                    } catch (err) {
                        console.error(err)
                    }
                }
            } catch (err) {
                console.error(err)
            }
        }
    }
]

exports.sync = () => {
    for (const job of jobs) {
        if (Array.isArray(job.cron)) {
            for (const time of job.cron) {
                cron.schedule(time, job.method)
                console.log(`Cron job ${job.name} initialized at ${time}`)
            }
        } else {
            cron.schedule(job.cron, job.method)
            console.log(`Cron job ${job.name} initialized at ${job.cron}`)
        }
    }
}
