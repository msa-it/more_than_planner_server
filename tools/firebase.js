const path = require("path");
const {FIREBASE_CREDENTIALS_PATH} = require('../config/config')

module.exports = new (class {
    constructor() {
        const configPath = path.resolve(__dirname, '../' + FIREBASE_CREDENTIALS_PATH)
        this.Config = require(configPath)
        this.App = this.Admin.initializeApp({
            credential: this.Admin.credential.cert(this.Config)
        })
        this.Messaging = this.Admin.messaging(this.App)
    }

    Admin = require('firebase-admin')

    async send(token, notification) {
        return await this.Messaging.send({
            token,
            notification,
            android: {priority: 'high'}
        })
    }
})()
