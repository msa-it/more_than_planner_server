const Joi = require(`@hapi/joi`).extend(require('@hapi/joi-date'))
const { VALIDATION_SETTINGS } = require('../config/config')

exports.validate = async (params, schema, additionalValidationSettings = {}) => {
	let settings = Object.assign(
		{},
		VALIDATION_SETTINGS,
		additionalValidationSettings
	)
	if (!Joi.isSchema(schema)) {
		schema = exports.JoiObject(schema)
	}
	params = await schema.validateAsync(
		params,
		settings
	)
	return params
}

exports.JoiObject = (keys) => {
	return Joi.object().keys(keys)
}

exports.JoiArray = (items) => {
	return Joi.array().items(items)
}

exports.Joi = Joi
