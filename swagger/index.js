module.exports = async () => {
	const swaggerUi = require('swagger-ui-express')
	const app = require('../app')
	const YAML = require('yamljs')
	const swaggerDocument = YAML.load('swagger/swagger.yaml')

	app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))
	console.log('Swagger docs initialized')
	const helmet = require('helmet')
	app.use(helmet())
	console.log('Helmet initialized')
}
